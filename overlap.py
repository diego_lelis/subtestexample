def error_hand_overlap(line1, line2):
    """
    Raises exception if line1 or line2 are invalid
    :param line1: See :attr:``line1`` in :func:`overlap`.
    :param line2: See :attr:``line2`` in :func:`overlap`.
    :return:
    """
    lines = [line1, line2]
    for i, line in enumerate(lines):
        if not line \
                or len(line) != 2 \
                or not str(line[0]).replace("-", "").isdigit() \
                or not str(line[1]).replace("-", "").isdigit():
            raise Exception(f"Line {i+1} is invalid")


def overlap(line1, line2, ordered=True):
    """
    Checks if two lines on the x-axis overlap
    :param line1: line on the format (x1,x2) or [x1, x2] - x1 and x2 are integers
    :param line2: line on the format (x2,x3) or [x2, x3] - x2 and x3 are integers
    :param ordered: Informs if the line limits are sorted, if not, it will sort before processing default: True
    :return: True if overlap, False if do not overlap
    """
    error_hand_overlap(line1, line2)
    l1 = line1
    l2 = line2
    if not ordered:
        l1 = sorted(line1)
        l2 = sorted(line2)

    if l2[0] <= l1[0] <= l2[1] \
            or l2[0] <= l1[1] <= l2[1]:
        return True
    elif l1[0] <= l2[0] <= l1[1] \
            or l1[0] <= l2[1] <= l1[1]:
        return True
    return False
