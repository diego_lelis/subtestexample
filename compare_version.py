def gen_version_exception(version_str):
    return Exception(f"Invalid version: {version_str}")


def get_value(arr, index, n, original_version):
    if index < n:
        value = arr[index]
        if not value.isdigit():
            raise gen_version_exception(original_version)
        return int(value)
    else:
        return 0


def error_hand_invalid_input(v1, v2):
    versions = [v1, v2]
    for version in versions:
        if not version:
            raise gen_version_exception(version)


def compare_version(v1, v2):
    error_hand_invalid_input(v1, v2)
    v1_arr = v1.split(".")
    v2_arr = v2.split(".")
    n1 = len(v1_arr)
    n2 = len(v2_arr)
    index = 0
    max_index = max(n1, n2)

    while index < max_index:
        val1 = get_value(v1_arr, index, n1, v1)
        val2 = get_value(v2_arr, index, n2, v2)
        if val1 > val2:
            return 1
        elif val1 < val2:
            return -1
        index += 1
    return 0
