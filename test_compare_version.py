from unittest import TestCase
from .compare_version import compare_version

test_data_first_greater = [
    ("1.2", "1.1"),
    ("2.2", "1.1"),
    ("1.2", "1.0"),
    ("1.2", "1"),
    ("1.2", "1.1.0"),
    ("1.1.2", "1.1"),
    ("1.2.1", "1.2"),
    ("1.2.1", "1.2.0"),
    ("10.0", "1.2.0"),
    ("10.10", "10.1"),
]

test_data_equal = [
    ("1", "1.0.0"),
    ("2.0", "2.0.0"),
    ("5", "5"),
    ("5.5", "5.5.0"),
    ("10.0", "10"),
]


test_data_second_greater = [
    ("1.1", "1.2"),
    ("1.1", "2.2"),
    ("1.0", "1.2"),
    ("1", "1.2"),
    ("1.1.0", "1.2"),
    ("1.1", "1.1.2"),
    ("1.2", "1.2.1"),
    ("1.2.0", "1.2.1"),
    ("1.2.0", "10.0"),
    ("10.1", "10.10"),
]


test_data_invalid_version = [
    ("1.a", "1.2"),
    ("", "2.2"),
    ("1.0", ""),
    ("1", None),
    ("1.1.0", "1.2.c"),
    ("1.1", "1.1.A"),
    ("1.2", "1.2.#"),
    ("a", "1.2.1"),
    ("f", "10.0"),
    (None, ""),
]


class TestCompareVersion(TestCase):
    def test_compare_version_first_greater(self):
        for v1, v2 in test_data_first_greater:
            with self.subTest(msg=f"Comparing first greater with v1:{v1} and v2:{v2}"):
                self.assertEqual(
                    1,
                    compare_version(v1, v2),
                    msg=f"Error on comparison for first greater with v1:{v1}, v2: {v2}")

    def test_compare_version_equal(self):
        for v1, v2 in test_data_equal:
            with self.subTest(msg=f"Comparing equal v1:{v1} and v2:{v2}"):
                self.assertEqual(
                    0,
                    compare_version(v1, v2),
                    msg=f"Error on comparison for equal v1:{v1}, v2:{v2}")

    def test_compare_version_second_greater(self):
        for v1, v2 in test_data_second_greater:
            with self.subTest(msg=f"Comparing second greater with v1:{v1} and v2:{v2}"):
                self.assertEqual(
                    -1,
                    compare_version(v1, v2),
                    msg=f"Error on comparison for second greater with v1:{v1}, v2:{v2}")

    def test_compare_version_throws_exception(self):
        for v1, v2 in test_data_second_greater:
            with self.subTest(msg=f"Comparing invalid version with v1:{v1} and v2:{v2}"):
                self.assertRaises(
                    Exception,
                    compare_version,
                    v1,
                    v2,
                    msg=f"Error on comparison for invalid version with v1:{v1}, v2:{v2}")
