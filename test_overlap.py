from unittest import TestCase

from .overlap import overlap

ordered_overlap_test_data = [
    # (line1, line2),
    # Positives
    ((1, 4), (0, 1)),
    ((1, 4), (0, 2)),
    ((1, 4), (1, 1)),
    ((1, 4), (2, 3)),
    ((1, 4), (2, 6)),
    ((1, 4), (4, 4)),
    ((1, 4), (4, 6)),
    # Points
    ((1, 1), (1, 1)),
    ((0, 0), (0, 0)),
    ((-1, -1), (-1, -1)),
    # Negatives
    ((-10, -4), (-15, -10)),
    ((-10, -4), (-15, -8)),
    ((-10, -4), (-10, -10)),
    ((-10, -4), (-9, -5)),
    ((-10, -4), (-9, -3)),
    ((-10, -4), (-4, -4)),
    ((-10, -4), (-4, -1)),
]

unordered_overlap_test_data = [
    # (line1, line2),
    # Positives
    ((1, 4), (1, 0)),
    ((1, 4), (2, 0)),
    ((1, 4), (1, 1)),
    ((1, 4), (3, 2)),
    ((4, 1), (2, 6)),
    ((4, 1), (4, 4)),
    ((1, 4), (6, 4)),
    # Points
    ((1, 1), (1, 1)),
    ((0, 0), (0, 0)),
    ((-1, -1), (-1, -1)),
    # Negatives
    ((-4, -10), (-15, -10)),
    ((-4, -10), (-8, -15)),
    ((-4, -10), (-10, -10)),
    ((-10, -4), (-5, -9)),
    ((-4, -10), (-9, -3)),
    ((-4, -10), (-4, -4)),
    ((-10, -4), (-1, -4)),
]

ordered_not_overlap_test_data = [
    ((2, 4), (-100, -20)),
    ((2, 4), (-15, 0)),
    ((2, 4), (0, 1)),
    ((2, 4), (1, 1)),
    ((2, 4), (5, 6)),
    ((1, 4), (25, 100)),
    # Points
    ((0, 0), (1, 1)),
    ((-1, -1), (-10, -10)),
    ((5, 5), (100, 100)),
    # Negatives
    ((-10, -4), (-100, -20)),
    ((-10, -4), (-15, -11)),
    ((-10, -4), (-11, -11)),
    ((-10, -4), (-3, -3)),
    ((-10, -4), (-3, 5)),
    ((-10, -4), (4, 10)),
    ((-10, -4), (100, 200)),
]


unordered_not_overlap_test_data = [
    ((4, 2), (-20, -100)),
    ((4, 2), (-15, 0)),
    ((2, 4), (1, 0)),
    ((4, 2), (1, 1)),
    ((4, 2), (5, 5)),
    ((4, 1), (100, 25)),
    # Points
    ((0, 0), (1, 1)),
    ((-1, -1), (-10, -10)),
    ((5, 5), (100, 100)),
    # Negatives
    ((-4, -10), (-20, -100)),
    ((-4, -10), (-15, -11)),
    ((-4, -10), (-11, -11)),
    ((-4, -10), (-3, -3)),
    ((-4, -10), (5, -3)),
    ((-10, -4), (4, 10)),
    ((-4, -10), (200, 100)),
]

invalid_first_line_test_data = [
    # Line 1
    ((1, 'a'), (1, 2)),
    (('a', 'a'), (1, 2)),
    ((1, 'a'), (1, 2)),
    ([1], (1, 2)),
    ((), (1, 2)),
]

class TestOverlap(TestCase):
    def test_overlap_ordered(self):
        for line1, line2 in ordered_overlap_test_data:
            with self.subTest(msg=f"Testing overlaps ordered {line1} and {line2}"):
                self.assertTrue(overlap(line1, line2),
                                msg=f"Ordered overlap error: {line1} x {line2}"
                                )
            with self.subTest(msg=f"Testing overlaps ordered {line2} and {line1}"):
                self.assertTrue(overlap(line2, line1),
                                msg=f"Ordered overlap error: {line2} x {line1}"
                                )

    def test_overlap_unordered(self):
        for line1, line2 in unordered_overlap_test_data:

            with self.subTest(msg=f"Testing overlaps unordered {line1} and {line2}"):
                self.assertTrue(overlap(line1, line2, ordered=False),
                                msg=f"Unordered overlap error: {line1} x {line2}"

                                )
            with self.subTest(msg=f"Testing overlaps unordered {line2} and {line1}"):
                self.assertTrue(overlap(line2, line1, ordered=False),
                                msg=f"Unordered overlap error: {line2} x {line1}"
                                )

    def test_not_overlap_ordered(self):
        for line1, line2 in ordered_not_overlap_test_data:

            with self.subTest(msg=f"Testing not overlaps ordered {line1} and {line2}"):
                self.assertFalse(overlap(line1, line2),
                                 msg=f"Ordered not overlap error: {line1} x {line2}"
                                 )

            with self.subTest(msg=f"Testing not overlaps ordered {line2} and {line1}"):
                self.assertFalse(overlap(line2, line1),
                                 msg=f"Ordered not overlap error: {line2} x {line1}"
                                 )

    def test_not_overlap_unordered(self):
        for line1, line2 in unordered_not_overlap_test_data:
            with self.subTest(msg=f"Testing not overlaps unordered {line1} and {line2}"):
                self.assertFalse(overlap(line1, line2, ordered=False),
                                 msg=f"Unordered not overlap error: {line1} x {line2}"
                                 )

                with self.subTest(msg=f"Testing not overlaps unordered {line2} and {line1}"):
                    self.assertFalse(overlap(line2, line1, ordered=False),
                                     msg=f"Unordered not overlap error: {line2} x {line1}"
                                     )

    def test_overlap_throws_exception(self):
        for line1, line2 in invalid_first_line_test_data:

            with self.subTest(msg=f"Testing invalid line1: {line1} x {line2}"):
                self.assertRaises(Exception, overlap, line1, line2)

            with self.subTest(msg=f"Testing invalid line2: {line2} x {line1}"):
                self.assertRaises(Exception, overlap, line2, line1)

